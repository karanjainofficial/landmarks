//
//  LandmarksApp.swift
//  Landmarks
//
//  Created by Puru on 2/22/21.
//

import SwiftUI

@main
struct LandmarksApp: App {
    
    @State private var modelData = ModelData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
            .environmentObject(modelData)
        }
    }
}
